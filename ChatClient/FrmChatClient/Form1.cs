﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmChatClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            adrIpLocale = getAdrIpLocaleV4();
            separateur = new char[1];
            separateur[0] = '#';
        }
        private IPAddress adrIpLocale;
        private char[] separateur;
        private IPAddress ipServeur = IPAddress.Parse("192.168.0.44");
        private int portServeur = 33000;
        private int portClient = 33001;
        private int lgMessage = 1000;
        private Socket sockReception;
        private IPEndPoint epRecepteur;
        byte[] messageBytes;
        private IPAddress getAdrIpLocaleV4()
        {
            string hote = Dns.GetHostName();
            IPHostEntry ipLocales = Dns.GetHostEntry(hote);
            foreach (IPAddress ip in ipLocales.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            return null; // aucune adresse IP V4
        }
      
        private void btnEnvoyer_Click(object sender, EventArgs e)
        {
            envoyer("E", tb_message.Text);
        }

        private void envoyer(string typeMessage, string texte)
        {
            byte[] messageBytes;
            Socket sock = new Socket(AddressFamily.InterNetwork,SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint epEmetteur = new IPEndPoint(adrIpLocale, 0);
            sock.Bind(epEmetteur);
            IPEndPoint epRecepteur = new IPEndPoint(ipServeur, portServeur);
            string infos = typeMessage + "#" + tb_pseudo.Text+ "#" + texte + "#";
            messageBytes = Encoding.Unicode.GetBytes(infos);
            sock.SendTo(messageBytes, epRecepteur);
            sock.Close();
            tb_message.Clear();
            tb_message.Focus();
        }

        private void btnConnecter_Click(object sender, EventArgs e)
        {
            if (tb_pseudo.Text != "")
            {
                btnConnecter.Enabled = false;
                tb_pseudo.Enabled = false;
                envoyer("C", "");
                btnConnecter.Enabled = true;
                tb_message.Enabled = true;
                initReception();
                tb_message.Focus();
            }
        }
        private void initReception()
        {
            messageBytes = new byte[lgMessage];
            sockReception = new Socket(AddressFamily.InterNetwork,
            SocketType.Dgram, ProtocolType.Udp);
            epRecepteur = new IPEndPoint(adrIpLocale, portClient);
            sockReception.Bind(epRecepteur);
            EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
            sockReception.BeginReceiveFrom(messageBytes, 0,lgMessage, SocketFlags.None,ref epTemp,new AsyncCallback(recevoir),null);
        }

        private void recevoir(IAsyncResult AR)
        {
            EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
            sockReception.EndReceiveFrom(AR, ref epTemp);
            string strMessage = Encoding.Unicode.GetString(messageBytes, 0,
            messageBytes.Length);
            string[] tabElements;
            tabElements = strMessage.Split(separateur);
            switch (tabElements[0])
            {
                case "R":
                    lbChat.Items.Add(tabElements[1]
                    + " -> " + tabElements[2]);
                    break;
            }
            Array.Clear(messageBytes, 0, messageBytes.Length);
            sockReception.BeginReceiveFrom(messageBytes, 0,
            lgMessage, SocketFlags.None,
            ref epTemp,
            new AsyncCallback(recevoir),
            null);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            envoyer("D", "");
        }
    }
}
