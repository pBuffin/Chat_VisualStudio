﻿namespace FrmChatServer
{
    partial class FremChatServer
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_pseudo = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lb_pseudo
            // 
            this.lb_pseudo.FormattingEnabled = true;
            this.lb_pseudo.Location = new System.Drawing.Point(12, 12);
            this.lb_pseudo.Name = "lb_pseudo";
            this.lb_pseudo.Size = new System.Drawing.Size(260, 238);
            this.lb_pseudo.TabIndex = 1;
            // 
            // FremChatServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lb_pseudo);
            this.Name = "FremChatServer";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lb_pseudo;
    }
}

