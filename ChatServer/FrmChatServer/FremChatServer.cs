﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmChatServer
{
    public partial class FremChatServer : Form
    {
        public FremChatServer()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            initReception();
        }
        private int lgMessage = 1000;
        private int portServeur = 33000;
        private int portClient = 33001;
        private IPAddress adrIpLocale;
        private char[] separateur;
        private Socket sockReception;
        private IPEndPoint epRecepteur;
        byte[] messageBytes;
        private IPAddress getAdrIpLocaleV4()
        {
            string hote = Dns.GetHostName();
            IPHostEntry ipLocales = Dns.GetHostEntry(hote);
            foreach (IPAddress ip in ipLocales.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            return null; // aucune adresse IP V4
        }


        private void initReception()
        {
            messageBytes = new byte[lgMessage];
            adrIpLocale = getAdrIpLocaleV4();
            separateur = new char[1];
            separateur[0] = '#';
            sockReception = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            epRecepteur = new IPEndPoint(adrIpLocale, portServeur);
            sockReception.Bind(epRecepteur);
            EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
            sockReception.BeginReceiveFrom(messageBytes, 0, lgMessage, SocketFlags.None, ref epTemp, new AsyncCallback(recevoir), null);
        }
        private void envoyerBroadcast(string pseudo, string texte)
        {
            byte[] messageBroadcast;
            Socket sockEmission = new Socket(AddressFamily.InterNetwork,SocketType.Dgram,ProtocolType.Udp);
            sockEmission.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
            IPEndPoint epEmetteur = new IPEndPoint(adrIpLocale, 0);
            sockEmission.Bind(epEmetteur);
            IPEndPoint epRecepteur = new IPEndPoint(IPAddress.Broadcast, portClient);
            string strMessage = "R" + "#" + pseudo + "#" + texte + "#";
            messageBroadcast = Encoding.Unicode.GetBytes(strMessage);
            sockEmission.SendTo(messageBroadcast, epRecepteur);
            sockEmission.Close();
        }
        private void recevoir(IAsyncResult AR)
        {
            EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
            sockReception.EndReceiveFrom(AR, ref epTemp);
            string strMessage = Encoding.Unicode.GetString(messageBytes, 0, messageBytes.Length);
            string[] tabElements;
            tabElements = strMessage.Split(separateur);
            switch (tabElements[0])
            {
                case "C":
                    lb_pseudo.Items.Add(tabElements[1]);
                    break;
                case "E":
                    envoyerBroadcast(tabElements[1], tabElements[2]);
                    break;
                case "D":
                    lb_pseudo.Items.Remove(tabElements[1]);
                    break;
            }
            Array.Clear(messageBytes, 0, messageBytes.Length);
            sockReception.BeginReceiveFrom(messageBytes, 0, lgMessage,SocketFlags.None,ref epTemp,new AsyncCallback(recevoir),null);
        }
    }
}
